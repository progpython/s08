# -*- coding: UTF-8 -*-
'''
Created on 2014-02-26
updated on 2022-07-16 -- Python 3.10
@author: Johnny Tsheke 
'''

def sumDiff(x,y):
    '''cette fonction vient du livre de réference -Zelle-'''
    sums = x + y
    diff = x - y
    return sums, diff

somme,difference = sumDiff(12,20)

print("somme = ",somme," difference = ",difference)
#ou encore
tup = sumDiff(12,20)

print("Le tuple retourné est :", tup)