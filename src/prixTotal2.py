# -*- coding: UTF-8 -*-
'''
Created on 2014-03-03
updated on 2022-07-16 -- Python 3.10
@author: Johnny Tsheke 
'''

articles=[{"article":"A","prix":200},{"article":"B","prix":75.4}] #variable globale

def prixTotal():
    '''articles est une liste des dictionnaires des articles et leurs prix
      la fonction retourne le prix total de tous les articles
      Ici on utilise une variable globale: articles
    '''
    total=0 #variable locale dans la fonction
    for art in articles: # ici c'est la variable globale  articles 
        total= total + art["prix"] #calcul du prix total
    #on doit retourner le total
    return(total)

prix=prixTotal() # affectation du résultat de la fonction à la variable prix
print("Le prix total est: ",prix)