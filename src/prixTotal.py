# -*- coding: UTF-8 -*-
'''
Created on 2014-02-26
updated on 2022-07-16 -- Python 3.10
@author: Johnny Tsheke
'''

def prixTotal(listeArt):
    '''listeArt est une liste des dictionnaires des articles et leurs prix'''
    '''la fonction retourne le prix total de tous les articles '''
    total=0
    for art in listeArt:
        total= total + art["prix"] #calcul du prix total
    #on doit retourner le total
    return(total)


articles=[{"article":"A","prix":200},{"article":"B","prix":75.4}]
prix=prixTotal(articles) # affectation du résultat de la fonction à la variable prix
print("Le prix total est: ",prix)