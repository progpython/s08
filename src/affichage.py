# -*- coding: UTF-8 -*-
'''
Created on 2014-02-26
updated on 2012-07-16 -- Python 3.10
@author: Johnny Tsheke
'''

def afficheElements(listeElem):
    """affiche les elements de la liste passé en arguments"""
    print("Voici les éléments de la liste:")
    for elem in listeElem:
        print(elem)

nombres=[12,40,-7,9,100,3]
afficheElements(nombres)
