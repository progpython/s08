# -*- coding: UTF-8 -*-
'''
Created on 3 mars 2014
updated on 2022-07-16 -- Python 3.10
@author: Johnny Tsheke 
Cet exemple vient du livre de réference -Zelle-
https://mcsp.wartburg.edu/zelle/python/
'''

def addInterest(balance, rate):
    newBalance = balance * (1 + rate)
    balance = newBalance #modification d'un paramètre

def test():
    amount = 1000
    rate = 0.05
    addInterest(amount, rate)
    print(amount) #imprime 1000. explication avec des boites (boxes) d'affectation


test()