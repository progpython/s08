# -*- coding: UTF-8 -*-
'''
Created on 3 mars 2014
updated on 2022-07-16 -- Python 3.10
@author: Johnny Tsheke 
Cet exemple vient du livre de réference -Zelle-
'''

def addInterest(balances, rate):
    '''liste en argument'''
    for i in range(len(balances)):
        balances[i] = balances[i] * (1+rate)

def test():
    amounts = [1000, 2200, 800, 360]
    rate = 0.05
    addInterest(amounts, rate) #appel de la fonction  addInterest
    print(amounts) #imprime [1050.0, 2310.0, 840.0, 378.0]: boites (boxes) d'affectation


test() #appel de la fonction test