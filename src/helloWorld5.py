# -*- coding: UTF-8 -*-
'''
Created on 2014-02-26
updated on 2022-07-16 -- Python 3.10
@author: Johnny Tsheke 
'''

def helloWorld(message):
    """Programme avec argument affichant un message"""
    print(message)

msg="Hello world"   
helloWorld(msg) #appel de la fonction avec argument